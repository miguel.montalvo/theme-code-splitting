import Vue from 'vue'
import { mapState, mapMutations, mapGetters } from 'vuex'
import { set, unset } from 'lib/util'

import 'modules/cart-item/cart-item.vue'
import 'modules/quantity-selector/quantity-selector.vue'
import 'modules/image/image.vue'

Vue.component('cart-grid', {
  computed: {
    ...mapGetters(['subtotal']),
    ...mapState([
      'app',
      'loading',
      'isBootstrapped',
      'isMobile'
    ]),
    isEmpty () {
      return this.cart.items.length === 0
    },
    cart () {
      return this.app.cart
    }
  },
  watch: {
    'app.isMiniCartOpen' (value) {
      if (!value) {
        unset(document.body, 'overflow-hidden')
        return
      }

      set(document.body, 'overflow-hidden')
    },
    'app.cart' (cart) {
      window.BARREL.bus.$emit('cart', cart)
    }
  },
  methods: {
    ...mapMutations(['bootstrap']),
    ...mapMutations(['toggleMiniCart'])
  },
  mounted () {
    this.bootstrap()
  }
})
