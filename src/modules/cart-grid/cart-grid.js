import Vue from 'vue'
import store from './@store'
import 'modules/cart-grid/cart-grid.vue'

export default el => new Vue({el, store})
