import Vue from 'vue'
import on from 'dom-event'
import select from 'dom-select'
import throttle from 'lodash/throttle'
import { set, unset } from 'lib/util'

export default el => new Vue({
  el,
  data: {
    pageYOffset: 0,
    threshold: 0,
    itemCount: window.BARREL.cart.item_count
  },
  mounted () {
    this.threshold = this.$refs['nav-container'].offsetTop
    this.navContainerHeight = this.$refs['nav-container'].offsetHeight

    on(window, 'scroll', throttle(this.onScroll, 15))
    window.BARREL.bus.$on('cart', this.onCart)
  },
  computed: {
    isSticky () {
      return this.pageYOffset > this.threshold
    }
  },
  methods: {
    onScroll () {
      this.pageYOffset = window.pageYOffset

      if (!this.isSticky) {
        document.body.style.paddingTop = 0
        return
      }

      document.body.style.paddingTop = this.navContainerHeight + 'px'
    },
    openMiniCart () {
      window.BARREL.bus.$emit('open-mini-cart')
    },
    onCart (cart) {
      this.itemCount = cart.item_count
    }
  }
})
