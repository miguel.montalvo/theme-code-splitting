import Vue from 'vue'
import { mapState, mapMutations } from 'vuex'
import cartGrid from 'modules/cart-grid/cart-grid.vue'

Vue.component('mini-cart', {
  computed: {
    ...mapState(['app']),
    cart () {
      return this.app.cart
    }
  },
  created () {
    window.BARREL.bus.$on('open-mini-cart', () => {
      this.toggleMiniCart()
    })
  },
  methods: {
    ...mapMutations(['toggleMiniCart'])
  }
})
