import on from 'dom-event'
import Layzr from 'layzr.js'
import {isIEorEdge} from 'lib/util'

/* Setup LazyLoad */
const instance = Layzr({
  normal: 'data-normal',
  retina: 'data-retina',
  srcset: 'data-srcset',
  threshold: 0
})

instance
  .on('src:before', (image) => {
    on(image, 'load', (event) => {
      const container = image.parentNode

      /* Fallback for IE and Edge */
      if (isIEorEdge()) {
        let fallback = document.createElement('div')
        let backgroundImage = image.src
        if (!backgroundImage) {
          const srcset = image.srcset.split(' ')
          backgroundImage = srcset[srcset.length - 2]
        }
        fallback.style.backgroundImage = `url(${backgroundImage})`
        fallback.setAttribute('class', 'img__el img__el--ie js-fallback')
        container.replaceChild(fallback, image)
      }

      container.classList.add('is-loaded')
    })
  })

export default (el) => {
  instance
    .update()
    .check()
    .handlers(true)
}
